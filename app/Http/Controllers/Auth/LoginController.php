<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Laravel\Socialite\Two\InvalidStateException;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use App\User;
use Illuminate\Http\Request;
use Carbon\Carbon;
// use Socialite;
use Image;
use Auth;
use Validator;
use Session;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/profile';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        $this->middleware(function ($request,$next) {
            if ($request->query('next')){
                Session::put('url.intended',$request->query('next'));     
            }
                
    	    return $next($request);
        });
    }

    public function showLoginForm()
    {
        return view('auth.login');
        return redirect()->route('verification.notice');
    }

    public function login(Request $request)
    {
        if ($request->query('next')){
            Session::put('url.intended',$request->query('next'));     
        }
        
        $this->validateLogin($request);

        $user = User::where('email','=',$request->email)->first();

        if(!$user){
            $this->sendFailedLoginResponse($request);
        }
        
        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if($user){
            if(!$user->email_verified_at && Hash::check($request->password, $user->password)){
                $request->session()->put('loggedUser', $user);
                return redirect()->route('verification.notice');
            }
        }

        if ($this->attemptLogin($request)) {
            
            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    public function redirectTo(){
        return Session::has('url.intended') ? Session::get('url.intended') : $this->redirectTo;
    }

    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->flush();

        $request->session()->regenerate();

        return redirect(url()->previous());
    }

    // public function redirectToProvider($provider,Request $request)
    // {
    //     return Socialite::driver($provider)->redirect();
    // }

    // public function handleProviderCallback($provider,Request $request)
    // {
    //     if (!$request->has('code') || $request->has('denied')) {
    //         return redirect('login');
    //     }

    //     try
    //     {
    //         $socialUser = Socialite::driver($provider)->user(); 
    //     }
    //     catch(InvalidStateException $e){
    //         return redirect('login');
    //     }
    //     catch (Exception $e)
    //     {
    //         return redirect('/login');
    //     }

    //     $authUser = $this->findOrCreateUser($socialUser, $provider);
        
    //     if(!$authUser->email){
    //         $authUser->photo = $socialUser->avatar_original;
    //         Session::put('no_email_user',$authUser);          
    //         return view('auth.getemail')->with('user',$authUser);
    //     }

    //     if(!$authUser->photo){
    //         $path = $socialUser->avatar_original;
    //         $fileName = time().rand(111,999).'.'.'jpg';
    //         Image::make($path)->save(public_path('profile_images/' . $fileName));             
    //         $authUser->photo= $fileName;
    //         $authUser->save();
    //     }

    //     auth()->login($authUser,true);

    //     return redirect($this->redirectTo());
    // }

    // private function findOrCreateUser($user, $provider)
    // {
    //     $authUser = User::where('provider_id', $user->id)->first();
    //     if ($authUser) {
    //         return $authUser;
    //     }
    //     if(User::where('email', $user->email)->exists()){
    //         $u = User::where('email', $user->email)->first();
    //         if(!$u->provider){
    //             $u->provider_id = $user->id;
    //             $u->provider = $provider;
    //         }
            
    //         if(!$u->email_verified_at){
    //             $u->email_verified_at = Carbon::now();
    //             $u->save();
    //         }

    //         return $u;
    //     }
    //     else{
    //         $u = new User;
    //         $u->provider = $provider;
    //         $u->provider_id = $user->getId();
    //         $u->name = $user->getName();
    //         $u->email = $user->getEmail();
	    
    //         if($user->getEmail()){
    //             $u->email_verified_at = Carbon::now();
    //             $u->save();
    //         }

    //         return $u;
    //     }
    // }

    // public function setEmail(Request $request){

    //     $socialUser = Session::get('no_email_user');
        
    //     $validator = Validator::make($request->all(), [
    //         'email' => 'required|email|unique:users'
    //     ]);

    //     if ($validator->fails()) {
    //         return view('auth.getemail')->withErrors($validator)->with('user',$socialUser);
    //     }

    //     if(!$socialUser) return redirect('login');

    //     $socialUser->email = $request->email;

    //     if($socialUser->photo){
    //         $path = $socialUser->photo;
    //         $fileName = time().rand(111,999).'.'.'jpg';
    //         Image::make($path)->save(public_path('profile_images/' . $fileName));             
    //         $socialUser->photo= $fileName;
    //     }
    //     $socialUser->save();

    //     Session::forget('no_email_user');

        
    //     $socialUser->sendEmailVerificationNotification();

    //     $request->session()->put('loggedUser', $socialUser);
    //     return redirect()->route('verification.notice');
    // }
}
